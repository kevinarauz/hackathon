import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from './usuario';
//import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login-usuario',
  templateUrl: './login-usuario.component.html',
  styleUrls: ['./login-usuario.component.css']
})
export class LoginUsuarioComponent {
  @ViewChild('alert', { static: true }) alert!: ElementRef;
  cargarLogo: boolean = false;
  error: boolean = false;
  intentos: any = 0;
  usuario: Usuario = new Usuario();
  loginForm!: FormGroup;

  constructor(private router:Router, private builder: FormBuilder) {
    this.usuario = new Usuario();
      /*this.loginForm = this.builder.group({
      usuario: new FormControl('', Validators.required),
      contrasena: new FormControl('', Validators.required)
    });*/
  }

  errorStatus:boolean = false;
  errorMsj: any = "";

  ngOnInit(): void {

  }

  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }

  onLogin(form: FormGroup){
    this.cargarLogo = true;
    
    this.router.navigate(['/cargaArchivo']);
    //this.toastr.success(`Hola ${this.usuario}, has iniciado sesión con éxito!`+"Login");
    
    this.cargarLogo = false;
  }
}
