import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginUsuarioComponent } from './login-usuario.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: LoginUsuarioComponent }
    ])],
    exports: [RouterModule]
})
export class LoginUsuarioRoutingModule { }
